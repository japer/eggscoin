package org.japer.eggscoin.api;

import org.japer.eggscoin.Block;
import org.japer.eggscoin.Transaction;
import org.japer.eggscoin.protocol.Request;
import org.japer.eggscoin.util.LambdaUtil;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class RemoteServerAPI implements ServerAPI {
    private String address;
    private int port;

    public RemoteServerAPI(String address, int port) {
        this.address = address;
        this.port = port;
    }

    private Object sendRequest(Request request) {
        try (Socket socket = new Socket(address, port);
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream())) {
            objectOutputStream.writeObject(request);
            return objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getBlockCount() {
        return (int) sendRequest(ServerAPI::getBlockCount);
    }

    @Override
    public byte[] getBlockDigest(int index) {
        return (byte[]) sendRequest(api -> api.getBlockDigest(index));
    }

    @Override
    public Block readBlock(byte[] digest) {
        return (Block) sendRequest(api -> api.readBlock(digest));
    }

    @Override
    public void writeBlock(Block block) {
        sendRequest(LambdaUtil.fromVoid(api -> api.writeBlock(block)));
    }

    @Override
    public int getTransactionCount() {
        return (int) sendRequest(ServerAPI::getTransactionCount);
    }

    @Override
    public byte[] getTransactionDigest(int index) {
        return (byte[]) sendRequest(api -> api.getTransactionDigest(index));
    }

    @Override
    public Transaction readTransaction(byte[] digest) {
        return (Transaction) sendRequest(api -> api.readTransaction(digest));
    }

    @Override
    public void writeTransaction(Transaction transaction) {
        sendRequest(LambdaUtil.fromVoid(api -> api.writeTransaction(transaction)));
    }
}

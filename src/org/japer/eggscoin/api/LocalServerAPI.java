package org.japer.eggscoin.api;

import org.japer.eggscoin.Block;
import org.japer.eggscoin.Transaction;

import java.security.MessageDigest;
import java.util.List;
import java.util.NoSuchElementException;

public class LocalServerAPI implements ServerAPI {
    private List<Block> blockChain;
    private List<Transaction> memoryPool;

    public LocalServerAPI(List<Block> blockChain, List<Transaction> memoryPool) {
        this.blockChain = blockChain;
        this.memoryPool = memoryPool;
    }

    @Override
    public int getBlockCount() {
        return blockChain.size();
    }

    @Override
    public byte[] getBlockDigest(int index) {
        return blockChain.get(index).getDigest();
    }

    @Override
    public Block readBlock(byte[] digest) {
        return blockChain
                .stream()
                .filter(block -> MessageDigest.isEqual(digest, block.getDigest()))
                .findAny()
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public void writeBlock(Block block) {
        blockChain.add(block);
    }

    @Override
    public int getTransactionCount() {
        return memoryPool.size();
    }

    @Override
    public byte[] getTransactionDigest(int index) {
        return blockChain.get(index).getDigest();
    }

    @Override
    public Transaction readTransaction(byte[] digest) {
        return memoryPool
                .stream()
                .filter(transaction -> MessageDigest.isEqual(digest, transaction.getDigest()))
                .findAny()
                .orElseThrow(NoSuchElementException::new);
    }

    @Override
    public void writeTransaction(Transaction transaction) {
        memoryPool.add(transaction);
    }
}

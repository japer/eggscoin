package org.japer.eggscoin.api;

import org.japer.eggscoin.Block;
import org.japer.eggscoin.Transaction;

public interface ServerAPI {
    int getBlockCount();
    byte[] getBlockDigest(int index);
    Block readBlock(byte[] digest);
    void writeBlock(Block block);

    int getTransactionCount();
    byte[] getTransactionDigest(int index);
    Transaction readTransaction(byte[] digest);
    void writeTransaction(Transaction transaction);
}

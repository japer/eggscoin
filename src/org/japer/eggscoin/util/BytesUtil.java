package org.japer.eggscoin.util;

public final class BytesUtil {
    public static String toHexString(byte[] bytes) {
        final char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int i = 0; i < bytes.length; i++) {
            int n = bytes[i] & 0xFF;
            hexChars[i * 2] = hexArray[n >>> 4];
            hexChars[i * 2 + 1] = hexArray[n & 0x0F];
        }
        return new String(hexChars);
    }
}

package org.japer.eggscoin.util;

import org.japer.eggscoin.protocol.Request;
import org.japer.eggscoin.api.ServerAPI;

import java.util.function.Consumer;

public class LambdaUtil {
    public static Request fromVoid(Consumer<ServerAPI> apiConsumer) {
        return api -> {
            apiConsumer.accept(api);
            return null;
        };
    }
}

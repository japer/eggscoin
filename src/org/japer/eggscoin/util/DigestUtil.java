package org.japer.eggscoin.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class DigestUtil {
    public static byte[] computeHash(Object object) {
        byte[] digest = null;
        try  {
            digest = MessageDigest.getInstance("SHA-256").digest(object.toString().getBytes());
        } catch (NoSuchAlgorithmException ignored) {
            assert false;
        }
        return digest;
    }
}

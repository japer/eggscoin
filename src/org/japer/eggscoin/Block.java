package org.japer.eggscoin;

import org.japer.eggscoin.util.BytesUtil;
import org.japer.eggscoin.util.DigestUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Block implements Serializable {
    private byte[] previousDigest;
    private List<Transaction> transactionList;
    private int nonce;

    public Block(byte[] previousDigest) {
        this(previousDigest, new ArrayList<>(), 0);
    }

    public Block(byte[] previousDigest, List<Transaction> transactionList, int nonce) {
        this.previousDigest = Objects.requireNonNull(previousDigest);
        this.transactionList = Objects.requireNonNull(transactionList);
        this.nonce = nonce;
    }

    public byte[] getPreviousDigest() {
        return previousDigest;
    }

    public List<Transaction> getTransactionList() {
        return transactionList;
    }

    public int getNonce() {
        return nonce;
    }

    public void setNonce(int nonce) {
        this.nonce = nonce;
    }

    public byte[] getDigest() {
        StringBuilder builder = new StringBuilder();
        builder.append(BytesUtil.toHexString(previousDigest));
        builder.append(':');
        for (Transaction transaction : transactionList) {
            builder.append(BytesUtil.toHexString(transaction.getDigest()));
            builder.append(':');
        }
        builder.append(nonce);
        return DigestUtil.computeHash(builder.toString());
    }
}

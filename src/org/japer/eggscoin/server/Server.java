package org.japer.eggscoin.server;

import org.japer.eggscoin.api.ServerAPI;
import org.japer.eggscoin.protocol.Request;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private int port;
    private ServerAPI api;
    private ServerSocket socket;

    public Server(int port, ServerAPI api, ServerSocket socket) {
        this.port = port;
        this.api = api;
        this.socket = socket;
    }

    public void loop() {
        //noinspection InfiniteLoopStatement
        while (true) {
            try {
                handleSocket(socket.accept());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleSocket(Socket socket) {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
             ObjectInputStream objectInputStream = new ObjectInputStream(socket.getInputStream())) {
            objectOutputStream.writeObject(((Request) objectInputStream.readObject()).execute(api));
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}

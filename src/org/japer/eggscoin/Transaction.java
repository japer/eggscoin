package org.japer.eggscoin;

import org.japer.eggscoin.util.DigestUtil;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class Transaction implements Serializable {
    private String sender;
    private String receiver;
    private BigDecimal amount;
    private BigDecimal fee;

    public Transaction(String sender, String receiver, BigDecimal amount, BigDecimal fee) {
        this.sender = sender;
        this.receiver = receiver;
        this.amount = amount;
        this.fee = fee;
    }

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public byte[] getDigest() {
        return DigestUtil.computeHash(
                Stream.of(sender,
                        receiver,
                        amount.toString(),
                        fee.toString()
                ).collect(Collectors.joining(":"))
        );
    }
}

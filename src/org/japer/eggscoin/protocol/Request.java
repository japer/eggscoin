package org.japer.eggscoin.protocol;

import org.japer.eggscoin.api.ServerAPI;

import java.io.Serializable;

public interface Request extends Serializable {
    Object execute(ServerAPI api);
}
